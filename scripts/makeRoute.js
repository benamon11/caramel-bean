const fs = require('fs');

if (fs.existsSync(`./routes/${process.argv[2]}`)) {
    console.log('Route exists');
} else {

fs.writeFileSync(`./routes/${process.argv[2]}.js`, `// Controllers
const express = require('express');
const router = express.Router();

module.exports = router;
`);

}