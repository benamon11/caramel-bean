const fs = require('fs');
if (fs.existsSync('./models')) {
    if (fs.existsSync(`./models/${process.argv[2]}`)) {
        console.log('model already exists. Please name it something else');
    } else {
        fs.writeFileSync(`./views/${process.argv[2]}.js`, '');
    }
} else {
    console.log('Creating model directory...');
    fs.mkdirSync('./models');
    fs.writeFileSync(`./models/${process.argv[2]}.js`, `const mongoose = require('mongoose');
    
    const ${process.argv[2]}Schema = new mongoose.Schema({

    });
    
    const ${process.argv[2]} = mongoose.model('${process.argv[2]}', ${process.argv[2]}Schema);
    module.exports = ${process.argv[2]};`);
}
