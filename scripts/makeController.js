const fs = require('fs');

if (!fs.existsSync(`./controllers/${process.argv[3]}Controller.js`)) {
    const routerContent = 
        fs.readFileSync(`./routes/${process.argv[2]}.js`)
        .toString()
        .replace(`// Controllers`, `// Controllers
const ${process.argv[3]}Controller = require('../controllers/${process.argv[3]}Controller')`);

    fs.writeFileSync(`./routes/${process.argv[2]}.js`, routerContent);
    fs.writeFileSync(`./controllers/${process.argv[3]}Controller.js`, '');

} else {
    console.log('The controller already exists. The command should follow: \n npm run makeController <routeName> <controllerName>');
}

