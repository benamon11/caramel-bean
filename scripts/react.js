const fs = require('fs');
const { exec } = require('child_process');

const main = async (title) => {
    await exec('npm i -g create-react-app', (error, stdout, stderr) => {
        if (error) {
          console.error(`exec error: ${error}`);
        }
    });
    
    await exec('npx create-react-app ' + process.argv[2], async (error, stdout, stderr) => {
        if (error) {
            console.log(error);
        } else {
            let reactPackage = await JSON.parse(fs.readFileSync(`./${process.argv[2]}/package.json`).toString());
            reactPackage.proxy = "http://localhost:3000";
            await fs.writeFileSync(`./${process.argv[2]}/package.json`, JSON.stringify(reactPackage));
        }
    });
}

main(process.argv[2]);
