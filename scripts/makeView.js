const fs = require('fs');

if (fs.existsSync(`./views/${process.argv[2]}`)) {
    console.log('View already exists. Please name it something else');
} else {
    fs.writeFileSync(`./views/${process.argv[2]}.ejs`, '');
}