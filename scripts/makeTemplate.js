const fs = require('fs');

if (fs.existsSync(`./views/partials/${process.argv[2]}`)) {
    console.log('View already exists. Please name it something else');
} else {
    fs.writeFileSync(`./views/partials/${process.argv[2]}.ejs`, '');
}