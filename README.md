# Caramel Bean

## Description

This is caramel-bean, a nodeJS templating script.

## Contents:
init
auth
addReact
makeRoute
makeController
makeView
makeTemplate
dev

## dev
To use this, run the command:
npm run dev
this will run the server

## init
To use this, go into /caramel-bean and type 
npm start
this will create a starter template.

## auth
to add authentication, in this templating engine, passports and mongoDB are used, type in
npm run auth
It will ask you to input the MongoDB string, it should look something like this:
mongodb+srv://username:password@test-sqotg.mongodb.net/test?retryWrites=true&w=majority

## makeRoute
to add a route, run the command:
npm run makeRoute routeName

## makeController
to make a controller, run the command:
npm run makeController routeName controllerName

## makeView
to make a view, run the command:
npm run makeView viewName
will be made as an ejs file

## makeTemplate
to make a template, run the command:
npm run makeTemplate templateName
will be made as an ejs file

## dev
to run the server, runs on port 5000

## addReact
creates a react app